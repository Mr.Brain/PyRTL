# Class for calculating RTL values

import numpy as np
import datetime as dt
from geopy import distance
import matplotlib.pyplot as plt
import csv

import time as tm
import os
import os_utils


class RTL_time_manager():
    def __init__(self):
        
        self.calculation_modes = ["basic", "alternative1", "window_detrend"]
        self.calculation_mode = "basic"
        
        self.start_time = None                   # filtering before RTL calculations
        self.stop_time = None                    # filtering before RTL calculations
        self.time_step = None                    # temporal discretization, days
        self.catalog_start_date = None           # first event of the given catalog


        self.point_longitude = None              # point where to calculate RTL time series
        self.point_latitude = None
        
        self.eq_longitude = None                 # eqrthquake parameters
        self.eq_latitude = None
        self.eq_time = None
        self.eq_magnitude = None

        self.M_min = 0
        self.h_min = 0
        self.h_max = 0

        self.catalog_events = None                  # all events in catalog
        self.catalog_dates = None

        self.filtered_events = None                 # filtered by M_min, h_min, h_max
        self.filtered_dates = None
        
        self.cell_coords = (None, None)
        self.calc_events = None                     # filtered by -||- and calc radius
        self.calc_dates = None       
               
        self.time_points = None                  # all time points where raw values will be calculated. format - dt.datetime

        self.R = None                            # arrays with pure values
        self.T = None
        self.L = None

        self.detr_R = None                       # values after detrending and normalized (sigma = 1)
        self.detr_T = None
        self.detr_L = None

        self.raw_RTL = None                      # array with raw RTL values
        self.RTL = None                          # array with raw RTL values
        self.norm_RTL = None

        self.T_wind = 700                        # days, time window for T function
        self.catalog_name = ""

        # RTL CONSTANTS (default, can be changed using class method)

        self.r0 = 50    # km
        self.t0 = 365   # days
        self.l0 = 10    # km
        self.p = 1      # coef
        self.energy_scale = None

        self.calc_radius = 100               # calculation radius, km. events from the circle with self.calc_radius 
                                             # will be taken for the calculation in specified point

    def pre_calculation_checks(self):
        assert self.calculation_mode in self.calculation_modes, \
        "Invalid calculation mode selected ({})." \
        " Possible modes: {}".format(self.calculation_mode, self.calculation_modes)
        assert len(self.calc_dates) > 0, "Filtered calatog has 0 events"

    def create_base_arrays(self):
        self.time_points = []
        it = self.start_time
        while (self.stop_time - it).days > 0:
            self.time_points.append(it)
            it=it+dt.timedelta(self.time_step)

        print("Created {} time points".format(len(self.time_points)))
        self.R = np.zeros(len(self.time_points))
        self.T = np.zeros(len(self.time_points))
        self.L = np.zeros(len(self.time_points))
        self.raw_RTL = np.zeros(len(self.time_points))

    def len_fault(self, x, energy_scale):     # fault length in the event source
        assert(energy_scale in ['K', 'Mb', 'Ms', 'Mw'] and
               "ERROR: Unknown energy scale for earthquakes - {}".format(energy_scale))
        if energy_scale == 'K':                     
            a = 0.244                       # Riznechenko
            c = -2.266
        if energy_scale == 'Mb':            # Sadovsky 
            a = 0.8
            c = -4.07
        if energy_scale == 'Ms':            # Sadovsky 
            a = 0.5
            c = -2.07
        if energy_scale == 'Mw':            # Sadovsky 
            a = 0.5
            c = -2.04
        return 10 ** (a * x + c)

# Initial parameters configuration

    def configure_params(self, start_time, eq_time, time_step, point_longitude, point_latitude, 
                         M_min, h_min, h_max, r0=50, t0=365, l0=10, p=1, calc_rad=100, T_wind=720, energy_scale='Ms',
                         eq_longitude = None, eq_latitude = None, eq_mag = None):
        self.eq_magnitude = eq_mag
        if start_time == "CATALOG_START_TIME":
            self.start_time = self.catalog_start_date
        else:
            if start_time < self.catalog_start_date:
                print("WARNING: provided start_time is less than date of first event in catalog.\n" \
                      "Switching start_time to catalog start date")
                self.start_time = self.catalog_start_date
            else:
                self.start_time = start_time
                
        self.stop_time = eq_time
        self.eq_time = eq_time

        if type(self.start_time) == type(self.stop_time) == type(''):
          self.start_time = dt.datetime.strptime(self.start_time, "%Y%m%d")
          self.stop_time = dt.datetime.strptime(self.stop_time, "%Y%m%d")

        self.time_step = time_step             # days
        self.point_longitude = point_longitude # point where to calculate RTL time series
        self.point_latitude = point_latitude
        
        self.eq_longitude = eq_longitude
        self.eq_latitude = eq_latitude

        self.M_min = M_min
        self.h_min = h_min
        self.h_max = h_max
        self.r0 = r0
        self.t0 = t0
        self.l0 = l0
        self.p = p
        self.calc_radius = calc_rad
        self.T_wind = T_wind
        self.energy_scale = energy_scale

        print()
        print("Parameters configured")
        print("RTL start at: {}\n".format(self.start_time))

        self.create_base_arrays()
        return 0
    def set_catalog(self, filename):
        self.catalog_name = filename

    def read_catalog(self):                # reads data from earthquake catalogue
                                           # creates variable "catalog_dates" (date-time of each event) and
                                           # variable "catalog_events" (parameters of each event)
                                           
        assert((len(self.catalog_name) > 0) and "Error: check catalog name")
        with open(self.catalog_name) as f_in:
            raw_data = f_in.readlines()
        self.catalog_events = np.zeros((len(raw_data), 4))
        self.catalog_dates = []
        for i, line in enumerate(raw_data):
            line = line.split()
            self.catalog_dates.append(dt.datetime.strptime(''.join(line[:2]).split('.')[0], '%Y%m%d%H%M%S')) # date
            self.catalog_events[i][0] = float(line[2]) # lat
            self.catalog_events[i][1] = float(line[3]) # lon
            self.catalog_events[i][2] = float(line[4]) # depth
            self.catalog_events[i][3] = float(line[5]) # magnitude
        self.catalog_start_date = self.catalog_dates[0]
        self.catalog_dates = np.array(self.catalog_dates, dtype='datetime64[ms]')
        print("Discovered {} events in the source catalogue.".format(len(self.catalog_dates)))
        print("First date {}\nLast date {}\n".format(self.catalog_dates[0], self.catalog_dates[-1]))
        success = (len(self.catalog_dates)>0)
        
        # erase
        self.filtered_events = np.zeros(shape = 0)
        self.filtered_events = np.zeros(shape = 0, dtype='datetime64[ms]')
        
        self.calc_events = np.zeros(shape = 0)
        self.calc_dates = np.zeros(shape = 0, dtype='datetime64[ms]')
        
        return success

    def print_parameters(self):
        print("=" * 23 + " RTL CALCULATION PARAMETERS " + "=" * 23)
        print(self.get_settings_as_string())
        print("=" * 74)
        
    def get_settings_as_string(self):
        s = ""
        s+="H = {} - {} km   M_MIN = {}   Calc radius = {} km   Time step = {}\n".format(
            self.h_min, self.h_max, self.M_min, self.calc_radius, self.time_step)
        s+="r0 = {} km   t0 = {} days   l0 = {} km   Calculation mode: {}\n".format(
            round(self.r0, 1), round(self.t0, 1), round(self.l0,1), self.calculation_mode)
        s+="p = {}".format(self.p)
        return s

    def set_parameters_by_dict(self, param_dict):
        if "rtl_h_min" in param_dict:
            self.h_min = param_dict["rtl_h_min"]
        if "rtl_h_max" in param_dict:
            self.h_max = param_dict["rtl_h_max"]
        if "rtl_M_min" in param_dict:
            self.M_min = param_dict["rtl_M_min"]
        if "rtl_calc_radius" in param_dict:
            self.calc_radius = param_dict["rtl_calc_radius"]
        if "rtl_r0" in param_dict:
            self.r0 = param_dict["rtl_r0"]
        if "rtl_t0" in param_dict:
            self.t0 = param_dict["rtl_t0"]
        if "rtl_l0" in param_dict:
            self.l0 = param_dict["rtl_l0"]
        if "rtl_p" in param_dict:
            self.p = param_dict["rtl_p"]
            
        print("Parameters changed")
        self.filtered_dates = None
        self.filtered_events = None
        self.calc_dates = None
        self.calc_events = None

    def set_parameters(self, h_min=None, h_max=None, M_min=None, calc_radius=None,
                          r0 = None, t0 = None, l0 = None, p = None):
        if h_min !=None:
            self.h_min = h_min
        if h_max !=None:
            self.h_max = h_max
        if M_min !=None:
            self.M_min = M_min
        if calc_radius !=None:
            self.calc_radius = calc_radius
        if r0 !=None:
            self.r0 = r0
        if t0 != None:
            self.t0 = t0
        if l0 !=None:
            self.l0 = l0
        if p != None:
            self.p = p

        print("Parameters changed")
        
        self.filtered_dates = None
        self.filtered_events = None
        self.calc_dates = None
        self.calc_events = None

    def get_start_date(self):
        return (self.catalog_start_date)
    
    def reload_point(self, new_lon, new_lat): # changes the point for RTL calculation
        self.point_longitude = new_lon
        self.point_latitude = new_lat
        
        self.cell_coords = (self.point_latitude, self.point_longitude)   
        r = [distance.great_circle(self.cell_coords,
                                   (self.filtered_events[k][0], self.filtered_events[k][1])).km \
                                     for k in range(len(self.filtered_events))] 
        r = np.array(r)
        pre_cut = r<=self.calc_radius                                                               
        self.calc_events = self.filtered_events[pre_cut]
        self.calc_dates = self.filtered_dates[pre_cut]
        
    def filter_events(self, silent = False):
        
        if not silent:
            print("\nEnergy scale: {}\n".format(self.energy_scale))
            print("Filtering catalogue...\n")
            print("Size of the raw catalogue: {} events".format(len(self.catalog_dates)))
                                     
        filter_bool = (self.catalog_dates >= np.array((self.start_time), dtype='datetime64[ms]')) *\
          	(self.catalog_dates < np.array((self.stop_time), dtype='datetime64[ms]')) *\
          	(self.catalog_events[:, 3] >= self.M_min) *\
          	(self.catalog_events[:, 2] < self.h_max) *\
          	(self.catalog_events[:, 2] >= self.h_min)
        
        self.filtered_dates = self.catalog_dates[filter_bool]
        self.filtered_events = self.catalog_events[filter_bool]
        
        self.cell_coords = (self.point_latitude, self.point_longitude)   
        r = [distance.great_circle(self.cell_coords,(self.filtered_events[k][0], self.filtered_events[k][1])).km for k in range(len(self.filtered_events))] # take into account only those events
        r = np.array(r)
        pre_cut = r<=self.calc_radius                                                                # which lying in the calc radius
        self.calc_events = self.filtered_events[pre_cut]
        self.calc_dates = self.filtered_dates[pre_cut]
        
        if not silent:
            print("Size of the filtered catalogue: {} events".format(len(self.calc_dates)))
            print("First date {}\nLast date {}\n".format(self.calc_dates[0], self.calc_dates[-1]))


    def calc_R(self, silent = False):     # R function calculation
        start_ind, stop_ind = 0, 0
        for it in range(1, len(self.time_points)):
            j = start_ind
            while  (j<len(self.calc_dates)) and (self.calc_dates[j] <= self.time_points[it]):
                j+=1
            
            stop_ind = j
            res_events = self.calc_events[start_ind:stop_ind]

            r = [distance.great_circle(self.cell_coords,(res_events[k][0], res_events[k][1])).km for k in range(len(res_events))]
            r = np.array(r)
            self.R[it] = self.R[it-1] + np.sum(np.exp(-r/self.r0))
            
            start_ind = stop_ind
        if (silent == False):
            print("R_SUCC")


    def calc_T(self, silent = False): # T function calculation
        for it in range(len(self.time_points)):
            if self.time_points[it] - dt.timedelta(self.T_wind) > self.start_time:
                min_calc_time = self.time_points[it] - dt.timedelta(self.T_wind)
            else:
                min_calc_time = self.start_time

            cut_bool = (self.calc_dates >= min_calc_time)*(self.calc_dates <= self.time_points[it])
            res_dates  = self.calc_dates[cut_bool]

            if len(res_dates)>=1:
                t = np.datetime64(self.time_points[it]) - res_dates
                t = t.astype(dt.datetime)
                exp_powers = np.array(-t/dt.timedelta(days=self.t0), dtype='float')
                self.T[it] = np.sum(np.exp(exp_powers))
            else:
                self.T[it] = 0
        if (silent == False):
            print("T_SUCC")
        

    def calc_L(self, silent = False): # L function calculation
        start_ind, stop_ind = 0, 0
        for it in range(1, len(self.time_points)):
          
            j = start_ind
            while  (j<len(self.calc_dates)) and (self.calc_dates[j] <= self.time_points[it]):
                j+=1
            
            stop_ind = j
            res_events = self.calc_events[start_ind:stop_ind]

            l_arr = [self.len_fault(res_events[i][3], self.energy_scale) for i in range(len(res_events))]
            l_arr = np.array(l_arr, dtype='float')  
            self.L[it] = self.L[it-1] + np.sum((l_arr/self.l0)**self.p)
            start_ind = stop_ind
        if (silent == False):
            print("L_SUCC")


    def detrend_R(self, in_window = False):                          # substracting linear trend from R, T and L
        if in_window:
            self.debug_R = []
            self.detr_R = np.zeros(len(self.R))
            self.time_points = np.array(self.time_points)
            window_size = self.t0*5
            threshold_date = self.time_points[0] + dt.timedelta(window_size)
            start_it = 0
            for i in range(len(self.time_points)):
                if self.time_points[i] < threshold_date:
                    pass
                else:
                    start_date = self.time_points[i] - dt.timedelta(window_size)
                    cut_bool = (self.time_points > start_date) * \
                                (self.time_points <= self.time_points[i])
                    data_x = np.arange(len(self.R[cut_bool]))
                    k,b = np.polyfit(data_x, self.R[cut_bool], 1)
                    detrended_array = (self.R[cut_bool] - k*data_x - b)
                    self.debug_R.append(np.std(detrended_array))
                    if (np.std(detrended_array))!=0:
                        self.detr_R[i] = detrended_array[-1] / np.std(detrended_array)
        else:
            data_x = np.arange(len(self.R))
            k,b = np.polyfit(data_x, self.R, 1)
            self.detr_R = self.R - k*data_x - b
            if (np.std(self.detr_R))!=0:
               self.detr_R = self.detr_R/np.std(self.detr_R)


    def detrend_T(self, in_window = False):
        if in_window:
            self.debug_T = []
            self.detr_T = np.zeros(len(self.T))
            self.time_points = np.array(self.time_points)
            window_size = self.t0*5
            threshold_date = self.time_points[0] + dt.timedelta(window_size)
            start_it = 0
            for i in range(len(self.time_points)):
                if self.time_points[i] < threshold_date:
                    pass
                else:
                    start_date = self.time_points[i] - dt.timedelta(window_size)
                    cut_bool = (self.time_points > start_date) * \
                                (self.time_points <= self.time_points[i])
                    data_x = np.arange(len(self.T[cut_bool]))
                    k,b = np.polyfit(data_x, self.T[cut_bool], 1)
                    detrended_array = (self.T[cut_bool] - k*data_x - b)
                    self.debug_T.append(np.std(detrended_array))
                    if (np.std(detrended_array))!=0:
                        self.detr_T[i] = detrended_array[-1] / np.std(detrended_array)
        else:
            data_x = np.arange(len(self.T))
            k,b = np.polyfit(data_x, self.T, 1)
            self.detr_T = self.T - k*data_x - b
            if (np.std(self.detr_T))!=0:
               self.detr_T = self.detr_T/np.std(self.detr_T)

    def detrend_L(self, in_window = False):
        if in_window:
            self.debug_L = []
            self.detr_L = np.zeros(len(self.L))
            self.time_points = np.array(self.time_points)
            window_size = self.t0*5
            threshold_date = self.time_points[0] + dt.timedelta(window_size)
            start_it = 0
            for i in range(len(self.time_points)):
                if self.time_points[i] < threshold_date:
                    pass
                else:
                    start_date = self.time_points[i] - dt.timedelta(window_size)
                    cut_bool = (self.time_points > start_date) * \
                                (self.time_points <= self.time_points[i])
                    data_x = np.arange(len(self.L[cut_bool]))
                    k,b = np.polyfit(data_x, self.L[cut_bool], 1)
                    detrended_array = (self.L[cut_bool] - k*data_x - b)
                    self.debug_L.append(np.std(detrended_array))
                    if (np.std(detrended_array))!=0:
                        self.detr_L[i] = detrended_array[-1] / np.std(detrended_array)
        else:
            data_x = np.arange(len(self.L))
            k,b = np.polyfit(data_x, self.L, 1)
            self.detr_L = self.L - k*data_x - b
            if (np.std(self.detr_L))!=0:
               self.detr_L = self.detr_L/np.std(self.detr_L)
                         
           
    def normalize_RTL(self):                     # substracts mean value from RTL and normalizes RTL on its variance
        if (np.std(self.RTL)!=0):
           self.RTL = (self.RTL - np.mean(self.RTL))/np.std(self.RTL)

    def detrend_RTL(self):
        data_x = np.arange(len(self.RTL))
        k,b = np.polyfit(data_x, self.RTL, 1)
        self.RTL = self.RTL - k*data_x - b

    def calc_raw_RTL(self): # deprecated
        for it in range(len(self.time_points)):
            self.raw_RTL[it] = self.R[it]*self.T[it]*self.L[it]


    def calc_RTL(self):                          # calculating RTL from detrended functions R, T, L
        assert(len(self.detr_R) == len(self.detr_T) == len(self.detr_L))
        self.RTL = np.zeros(len(self.detr_R))
        for it in range(len(self.detr_R)):
            self.RTL[it] = self.detr_R[it]*self.detr_T[it]*self.detr_L[it]
    
    def calc_weighed_RTL(self):
        self.RTL = np.zeros(len(self.time_points))
        
        r_all = [distance.great_circle(self.cell_coords,(self.calc_events[k][0], self.calc_events[k][1])).km for k in range(len(self.calc_events))]
        r_all = np.array(r_all)
        
        l_all = [self.len_fault(self.calc_events[i][3], self.energy_scale) for i in range(len(self.calc_events))]
        l_all = np.array(l_all, dtype='float')  
        
        for it in range(len(self.time_points)):
            if self.time_points[it] - dt.timedelta(self.T_wind) > self.start_time:
                min_calc_time = self.time_points[it] - dt.timedelta(self.T_wind)
            else:
                min_calc_time = self.start_time

            cut_bool = (self.calc_dates >= min_calc_time)*(self.calc_dates <= self.time_points[it])

            res_dates  = self.calc_dates[cut_bool]
            temp_t = [self.time_points[it]]*len(res_dates)
            temp_t = np.array(temp_t, dtype='datetime64[ms]')
            
            r = r_all[cut_bool]
            l = l_all[cut_bool]
            
            if len(res_dates)>=1:
             t = temp_t - res_dates
             t = np.array(t.tolist())
             exp_powers = np.array(-t/dt.timedelta(days=self.t0), dtype='float')
             self.RTL[it] = np.sum(np.exp(-r/self.r0) * np.power((l/self.l0), self.p) * np.exp(exp_powers))
            else:
             self.RTL[it] = 0
            
        print("Weighed RTL calculated")

    
    def RTL_full_process(self, silent = False):
        tic0 = tm.perf_counter()
        # self.filter_events(silent)
        if self.calculation_mode == "window_detrend":
            self.time_points = np.array(self.time_points) # temporary hack
            self.calc_R(silent)
            self.calc_T(silent)
            self.calc_L(silent)
            self.detrend_R(in_window = True)
            self.detrend_T(in_window = True)
            self.detrend_L(in_window = True)
            self.calc_RTL()
            self.normalize_RTL()
        
        if self.calculation_mode == "basic":
            self.calc_R(silent)
            self.calc_T(silent)
            self.calc_L(silent)
            self.detrend_R()
            self.detrend_T()
            self.detrend_L()
            self.calc_RTL()
            self.normalize_RTL()
            
        if self.calculation_mode == "alternative1":
            self.calc_weighed_RTL()
            self.detrend_RTL()
            self.normalize_RTL()
        tic1 = tm.perf_counter()
        print("Calculation time: {} s".format(round(tic1-tic0 ,4)))
    
    def get_events_by_threshold(self, ks_threshold):
        magnitudes = self.catalog_events[:,3]
        ks_filter = magnitudes>=ks_threshold
        f_dates = self.catalog_dates[ks_filter]
        f_ks = self.catalog_events[:,3][ks_filter]
        f_lats = self.catalog_events[:,0][ks_filter]
        f_lons = self.catalog_events[:,1][ks_filter]
        return f_dates, f_lons, f_lats, f_ks

# Plotting functions


    def plot_R(self):
        plt.plot(self.time_points, self.R)
        plt.title("Raw R values")

    def plot_T(self):
        plt.plot(self.time_points, self.T)
        plt.title("Raw T values")

    def plot_L(self):
        plt.plot(self.time_points, self.L)
        plt.title("Raw L values")

    def plot_raw_RTL(self):
        fig, ax = plt.subplots()
        ax.plot(self.time_points, self.raw_RTL, 'black')
        ax.set_title("Raw RTL values", fontweight="bold")
        plt.xlabel('Time, year')
        plt.ylabel('RTL, raw values')

    def export_npz(self, time_format = "default"):
        if time_format not in ["default", "years"]:
            time_format = "default"
            print("Warning: incorrect time format for export. Switching to default")

        if time_format == "default":
            formatted_t = np.array(self.time_points)
        if time_format == "years":
            deltas = (np.array(self.time_points) - self.eq_time)
            formatted_t = [a.days / 365 for a in deltas]
            
        np.savez(os.path.join('results_storage', 'time_graphs','export','RTL_time_graph_{}-{}'.format(
            self.point_longitude, self.point_latitude)),
            t = formatted_t, rtl = self.RTL, 
            lon = self.point_longitude, lat = self.point_latitude,
            metainfo = self.get_settings_as_string())

    def export_csv(self):
        savepath = os.path.join('results_storage', 'time_graphs')
        if not os.path.exists(savepath):
            os.mkdir(savepath)
        if not os.path.exists(os.path.join(savepath, "pics")):
            os.mkdir(os.path.join(savepath, "pics"))
        if not os.path.exists(os.path.join(savepath, "params")):
            os.mkdir(os.path.join(savepath, "params"))
        if not os.path.exists(os.path.join(savepath, "export")):
            os.mkdir(os.path.join(savepath, "export"))
        # with open('results_storage/time_graphs/export/RTL_time_graph_{}-{}.csv'.format(
        #         self.point_longitude, self.point_latitude), 'w', newline='') as csvfile:
        #  csv_writer = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_NONE)
        #  csv_writer.writerow(["#date-time","RTL"])
        #  for it in range(len(self.time_points)):
        #      csv_writer.writerow([self.time_points[it].strftime("%Y%m%d-%H:%M:%S"), self.RTL[it]])
             
        with open(os.path.join('results_storage','time_graphs','export','RTL_time_graph_yrs_{}-{}.csv'.format(
                self.point_longitude, self.point_latitude)), 'w', newline='') as csvfile:
         csv_writer = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_NONE)
         csv_writer.writerow(["#time_years","RTL"])
         for it in range(len(self.time_points)):
             csv_writer.writerow([(self.time_points[it]-self.stop_time).days / 365, self.RTL[it]])

    def plot_RTL(self, eq_arrow = False, RTL_threshold=False, save_plot=False, 
                 silent = False, export_npz = False, years_before_eq = False, xlim_years = None):
        
        fig, ax = plt.subplots()
        if years_before_eq:
            ax.plot([(self.time_points[it]-self.stop_time).days / 365 for it in range(len(self.time_points))], self.RTL, 'black')
        else:
            ax.plot(self.time_points, self.RTL, 'black')
        
        ax.set_title("Longitude: {}, Latitude: {}, M: {}\nCatalog: {} Time: {}".format(self.point_longitude,
                                                        self.point_latitude,
                                                        self.eq_magnitude,
                                                        self.catalog_name.split('/' )[-1],
                                                        self.stop_time), fontsize = 18)
        fig.set_figwidth(12)
        fig.set_figheight(6)
        
        if years_before_eq:
            plt.xlabel('Time, years before earthquake')
            if xlim_years != None:
                plt.xlim(-xlim_years,)
        else:
            plt.xlabel('Time, year')
            
        plt.ylabel(r'RTL, $\sigma$')
        props = dict(boxstyle='round', facecolor='wheat', alpha=1)
        plt.text(0.01, 0.98, "H = {} - {} km\nM_MIN = {}\nCalc radius = {} km\nTime step = {} days\n$r_0$ = {} km, $t_0$={} yr\nTime window = {} yr\nEnergy scale = {}".format(self.h_min, 
                                                                                                  self.h_max, 
                                                                                                  self.M_min, 
                                                                                                  self.calc_radius, 
                                                                                                  self.time_step,
                                                                                                  self.r0,
                                                                                                  round(self.t0/365.0, 2),
                                                                                                  round(self.T_wind /365.0, 2),
                                                                                                  self.energy_scale), transform=ax.transAxes, fontsize = 12, verticalalignment='top', bbox=props)
        if RTL_threshold:
            plt.axhline(y=RTL_threshold, color='r', linestyle='--')
        if eq_arrow:
            if years_before_eq:
                plt.scatter(0, 0, marker='*', s=[180], color='red') 
            else:
                plt.scatter(self.stop_time, 0, marker='*', s=[180], color='red') 
            
        plt.grid()
        if save_plot:
            savepath = os.path.join('results_storage', 'time_graphs')
            if not os.path.exists(savepath):
                os.mkdir(savepath)
            if not os.path.exists(os.path.join(savepath, "pics")):
                os.mkdir(os.path.join(savepath, "pics"))
            if not os.path.exists(os.path.join(savepath, "params")):
                os.mkdir(os.path.join(savepath, "params"))
            if not os.path.exists(os.path.join(savepath, "export")):
                os.mkdir(os.path.join(savepath, "export"))
                
            curr_time = tm.time()
            plt.savefig(os.path.join(savepath, 'pics', '{}.png'.format( 
                        str(self.eq_time.strftime("%Y%m%d_%H%M%S")))), dpi=300)
                        
            last_date = self.time_points[-1] # determine if the RTL values reach RTL threshold during 2 last years
            delta2 = dt.timedelta(365*2)
            start_check_date = last_date - delta2
            time_filter = np.array(self.time_points)>start_check_date
            RTL_threshold_reached = "No info"
            if np.sum(self.RTL[time_filter]<-2)>0:
                RTL_threshold_reached = 1
            else:
                RTL_threshold_reached = 0
            
            f = open(os.path.join(savepath, 'params', str(curr_time)),'w')        
            f.write("CATALOG {}\nT_START {}\nT_STOP {}\nPOINT_LON {}\nPOINT_LAT {}\nHMIN {}\nHMAX {}\nM_MIN {}\n"\
                    "CALC_RADIUS(KM) {}\nTIME_STEP(DAYS) {}\nr0(KM) {}\nt0(YEARS) {}\n"\
                    "l0(KM) {}\nRTL_WARNING_2YEARS {}".format(self.catalog_name,
                                                                                                      self.start_time,
                                                                                                      self.stop_time,
                                                                                                      self.point_longitude,
                                                                                                      self.point_latitude,
                                                                                                      self.h_min, 
                                                                                                      self.h_max, 
                                                                                                      self.M_min, 
                                                                                                      self.calc_radius, 
                                                                                                      self.time_step,
                                                                                                      self.r0,
                                                                                                      self.t0/365.0,
                                                                                                      self.l0,
                                                                                                      RTL_threshold_reached))
            f.close()

        if not silent:
            plt.show()
        
    def get_eqs_params(self):
        assert(len(self.catalog_events)!=0)
        lons = self.catalog_events[:,1]
        lats = self.catalog_events[:,0]
        magnitudes = self.catalog_events[:,3]
        depths = self.catalog_events[:,2]
        times = self.catalog_dates
        return lons, lats, magnitudes, depths, times
    
    def apply_meta_data(self, metafile_path, catalogs_basepath = 'db_catalogs'):
        with open(metafile_path) as f:
            meta_lines = f.readlines()
        meta_lines = [line.split(':') for line in meta_lines]
        params_vocab = {}
        for line in meta_lines:
            if len(line) == 2:
                params_vocab[line[0]] = line[1][:-1]
            else:
                pass
        found_catalog_path,  = os_utils.passage(params_vocab["cat_name"], catalogs_basepath)
        print("Catalog {} found at {}".format(params_vocab["cat_name"], found_catalog_path))
        self.set_catalog(found_catalog_path + "/" + params_vocab["cat_name"])
        self.read_catalog()
        
        if (("calc_lon" in params_vocab) and ("calc_lat" in params_vocab)):
            calc_lon = float(params_vocab["calc_lon"])
            calc_lat = float(params_vocab["calc_lat"])
            print("Specific calculation point is used: {} {}".format(calc_lon, calc_lat))
        else:
            print("Default calculation point is used: EQ epicenter")
            calc_lon = float(params_vocab["eq_lon"])
            calc_lat = float(params_vocab["eq_lat"])
        
        if "rtl_p" in params_vocab:
            rtl_p = float(params_vocab["rtl_p"])
        else:
            rtl_p = 1
        
        if "rtl_start_time" in params_vocab:
            if params_vocab["rtl_start_time"] == "CATALOG_START_TIME":
                manager_start_time = "CATALOG_START_TIME"
            else:
                manager_start_time = dt.datetime.strptime(params_vocab["rtl_start_time"], "%Y%m%d")
        else:
            manager_start_time = "CATALOG_START_TIME"
            
        self.configure_params(manager_start_time, dt.datetime.strptime(params_vocab["eq_time"], "%Y%m%d"), 
                              15, calc_lon, calc_lat,
                              float(params_vocab["rtl_m_min"]), float(params_vocab["rtl_h_top"]), 
                              float(params_vocab["rtl_h_bottom"]), float(params_vocab["rtl_r0"]),
                              float(params_vocab["rtl_t0"]), float(params_vocab["rtl_l0"]), rtl_p,
                              float(params_vocab["rtl_calc_radius"]), float(params_vocab["rtl_T_window"]),
                              params_vocab["energy_scale"], params_vocab["eq_lon"], params_vocab["eq_lat"],
                              params_vocab["eq_M"])
        
