import os
import math
import datetime as dt
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt
from rtl_time_manager import RTL_time_manager

# ======================== USER-DEFINED PARAMETERS ======================

CAT_NAME = os.path.join('db_catalogs','Kamchatka','catemsd_all_noaft_2023.txt')
MAGNITUDE_THRESHOLD = 15 # for plotting
START_TIME = "19000101"

# =======================================================================

def create_meta_file(time, lon, lat, depth, magnitude):
    time_plain = str(time).split('T')[0].replace('-','')
    basepath_metafile = os.path.join('metafiles','auto')
    if not os.path.exists(basepath_metafile):
        os.mkdir(basepath_metafile)
        
    with open(os.path.join(basepath_metafile,'{}.meta'.format(time_plain)),'w') as f:
        f.write("cat_name:{}\n".format(CAT_NAME))
        f.write("energy_scale:{}\n".format('K'))
        
        f.write("eq_lat:{}\n".format(lat))
        f.write("eq_lon:{}\n".format(lon))
        
        f.write("eq_h:{}\n".format(depth))
        f.write("eq_time:{}\n".format(time_plain))
        f.write("eq_M:{}\n".format(magnitude))
        f.write("rtl_start_time:{}\n".format("CATALOG_START_TIME"))
        f.write("rtl_m_min:{}\n".format(9))
        f.write("rtl_h_top:{}\n".format(0))
        f.write("rtl_h_bottom:{}\n".format(100))
        f.write("rtl_r0:{}\n".format(50))
        f.write("rtl_t0:{}\n".format(365))
        f.write("rtl_l0:{}\n".format(10))
        f.write("rtl_T_window:{}\n".format(700))
        f.write("rtl_calc_radius:{}\n".format(130))

def dist(p1, p2):
    return math.hypot(p2[0] - p1[0], p2[1] - p1[1])

plt.close('all')
manager = RTL_time_manager()
manager.set_catalog(CAT_NAME)
manager.read_catalog()
lons, lats, magnitudes, depths, time_data = manager.get_eqs_params()

assert(len(lats)==len(lons)==len(magnitudes))

# initialize an axis
fig, ax = plt.subplots(figsize=(8,6))
# plot map on axis
countries = gpd.read_file(  
     os.path.join('shapefiles', 'NaturalEarth_MediumScale', 'ne_50m_land.shp'))

start_time = dt.datetime.strptime(START_TIME, "%Y%m%d")

bool_filter = (magnitudes>=MAGNITUDE_THRESHOLD) * (time_data >= start_time)

xdata = lons[bool_filter]
ydata = lats[bool_filter]
time_data = list(time_data[bool_filter])
depths = depths[bool_filter]
magnitudes = magnitudes[bool_filter]


annotate_data = time_data

assert(len(xdata)==len(ydata))

countries.plot(color="lightgreen",ax=ax)
ax.plot(xdata, ydata, 'o')
ax.legend(["Earthquakes with energy class >={}\nTime threshold: {}".format(
                                        MAGNITUDE_THRESHOLD, start_time)])

for i, txt in enumerate(annotate_data):
    ax.annotate(txt, (xdata[i], ydata[i]))

# add grid
ax.grid(alpha=0.5)

def onclick(event):
    if event.button == 3:
        click_x = event.xdata
        click_y = event.ydata
        dists = [dist((click_x, click_y),(xdata[i], ydata[i])) for i in range(len(xdata))]
        idx= np.argmin(dists)
        xmin, xmax = ax.xaxis.get_view_interval()
        ymin, ymax = ax.yaxis.get_view_interval()
        ax.plot(xdata[idx], ydata[idx], 'o', color = 'red')
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
        # with open(CAT_NAME[:CAT_NAME.rfind('/')] + "/target_events.csv",'a') as f:
        #     f.write("{} {} {} {}\n".format(time_data[idx], xdata[idx], ydata[idx], magnitudes[idx]))
        #     f.close()
        create_meta_file(time_data[idx], xdata[idx], ydata[idx], depths[idx], magnitudes[idx])
        
cid = fig.canvas.mpl_connect('button_press_event', onclick)
plt.show()



        

        
        
