
import geopandas as gpd
import csv
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, TextBox
import matplotlib
import time
from datetime import datetime as datetime_type
import os

font = {'family': 'Sans', 'size'   : 12}
boldfont = {'family': 'Sans', 'weight': 'bold',
        'size': 12}

matplotlib.rc('font', **font)

def plot_interactive(x1, x2, y1, y2, x_dots, y_dots, data_array, time_points, 
                     special_point, metainfo, shapefile, title1, title2, parameter_name, RTL_manager, point_label = ""):    
    maps_man = maps_manager()
    maps_man.set_map_boundaries(x1, x2, y1, y2)
    maps_man.set_grid_nodes(x_dots, y_dots)
    maps_man.set_target_data(data_array, time_points, metainfo, special_point)
    maps_man.set_titles(title1, title2, parameter_name)
    maps_man.set_point_label(point_label)
    maps_man.print_controls_info()
    maps_man.prepare_axes(shapefile)

class maps_manager:
    
    def __init__(self):
        self.text_flag = 0
        self.curr_it = 0
        self.xmin = None
        self.xmax = None
        self.ymin = None
        self.ymax = None
        self.x_points = None
        self.y_points = None
        self.data_array = None
        self.time_points = None
        self.metainfo = None
        self.special_point = None
        self.title1 = None
        self.title2 = None
        self.parameter_name = None
        self.fig, self.ax = plt.subplots(1,2, figsize = (18, 9), 
                                         constrained_layout=True)
        
    def print_controls_info(self):
        print()
        print("Press Z to save current map in NPZ format")
        print("Press E to save current map in CSV and PNG format")
        print("Press X to save all maps in NPZ format")
        
    def set_titles(self, t1, t2, parameter_name):
        self.title1 = t1
        self.title2 = t2
        self.parameter_name = parameter_name
    
    def set_point_label(self, label):
        self.special_point_label = label
    
    def set_map_boundaries(self, xmin, xmax, ymin, ymax):
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
    
    def set_grid_nodes(self, x_points, y_points):
        self.x_points = x_points
        self.y_points = y_points
        
    def set_target_data(self, data_array, time_points, metainfo, special_point = (None, None)):
        self.data_array = data_array
        self.time_points = time_points
        self.metainfo = metainfo
        self.special_point = special_point
        
    def prepare_axes(self, shapefile):
        if shapefile:
            self.countries = gpd.read_file(shapefile)
        else:
            self.countries = gpd.read_file(os.path.join('shapefiles','NaturalEarth_MediumScale','ne_50m_land.shp'))
            
        self.ax[1].set_box_aspect(0.3)
        self.prepare_ax0()
        self.ax[0].set_title(self.title1 + "\n{}".format(
            self.time_points[self.curr_it].astype(datetime_type)), fontweight="bold")
        self.ax0_base_children = self.ax[0].get_children()
        
        # UI elements
        
        # self.axapply = self.fig.add_axes([0.7, 0.08, 0.1, 0.075])
        # self.bapply = Button(self.axapply, 'Apply')
        # self.bapply.on_clicked(self.button_apply)
        cid = self.fig.canvas.mpl_connect('button_press_event', self.onclick)
        cid = self.fig.canvas.mpl_connect('key_press_event', self.on_press)
        
        self.slider_axcolor = 'lightgoldenrodyellow'
        self.axfreq = plt.axes([0.55, 0.05, 0.4, 0.03], facecolor=self.slider_axcolor)
        self.sfreq = Slider(self.axfreq, 'Time slider', 0, len(self.time_points)-1, valinit=self.curr_it, valstep=1)
        self.sfreq.on_changed(self.update)
        
        map_data = self.data_array[:,:,0]
        
        self.color_graph = self.ax[0].pcolor(self.x_points, self.y_points, map_data.transpose(), shading = 'auto', cmap='Blues_r', vmin = -2, vmax=0)
        self.cbar = self.fig.colorbar(self.color_graph, ax=self.ax[0], shrink=0.5)
        self.cbar.set_ticks([-2, 0])
        self.cbar.set_ticklabels([" - 2", "> 0"])
    
        self.ax_input = self.fig.add_axes([0.2, 0.0, 0.2, 0.03])
        self.text_box = TextBox(self.ax_input, "Custom EQ epicenter", textalignment="center")
        self.text_box.on_submit(self.update_eq_epicenter)
        # self.text_box.set_val("lon:lat")  # Trigger `submit` with the initial string
        plt.show()
        
    def update_eq_epicenter(self, expresion):
        try:
            data = expresion.split(':')
            data[0] = float(data[0])
            data[1] = float(data[1])
            self.special_point = tuple(data)
            self.special_point_label = ""
            
        except Exception:
            print('Incorrect format. Provide epicenter data in format longitude:latitude')
        
    def erase_ax0(self):
        ch = self.ax[0].get_children()
        for sample in ch:
            if sample not in self.ax0_base_children:
                sample.remove()
        
    def prepare_ax0(self):
        self.ax[0].set_xlabel('Longitude')
        self.ax[0].set_ylabel('Latitude')
        self.ax[0].set_xlim(self.xmin, self.xmax)
        self.ax[0].set_ylim(self.ymin, self.ymax)
        self.countries.plot(color="lightgreen",ax=self.ax[0])
        
    def button_apply(self, event):
        
        map_data = self.data_array[:,:,self.curr_it]
                
        self.erase_ax0()
        self.ax[0].set_title(self.title1 + "\n{}".format(self.time_points[self.curr_it].astype(datetime_type)),
                             fontweight="bold")
        self.ax[0].scatter(self.special_point[0], self.special_point[1], marker = '*', s = 100, color = 'red', zorder = 100)
        self.ax[0].annotate(self.special_point_label, self.special_point, color = "red")
        self.ax[0].pcolor(self.x_points, self.y_points, map_data.transpose(), alpha=0.5, 
                                   shading = 'auto', cmap='Blues_r', vmin = -2, vmax=0)
    
        if self.text_flag:
            for ix in range(len(self.x_points)):
                    for iy in range(len(self.y_points)):
                        self.ax[0].text(self.x_points[ix], self.y_points[iy], round(self.data_array[ix][iy][self.curr_it],1), 
                                        va = "center", ha = "center", alpha=0.5)
        plt.draw()
    
    
    
    def update(self, val):
        self.curr_it = int(self.sfreq.val)
        self.button_apply(None)
        
    def onclick(self, event):

        if (event.button == 1):
            if (event.inaxes == self.ax[0]):
                x = event.xdata
                y = event.ydata
                self.ax[1].clear()
                ix = np.argmin(abs(self.x_points - x))
                iy = np.argmin(abs(self.y_points - y))
                print(self.x_points[ix], self.y_points[iy])
                self.ax[1].plot(self.time_points, self.data_array[ix][iy], color = "black")
                self.ax[1].axhline(y=0, alpha = 0.3)
                self.ax[1].set_title(self.title2 + " ({}, {})".format(round(self.x_points[ix],3), 
                                    round(self.y_points[iy],3)), fontweight="bold")
                props = dict(boxstyle='round', facecolor='wheat', alpha=1)
                self.ax[1].text(0.5,-0.6, self.metainfo, ha="center", transform=self.ax[1].transAxes, bbox=props)
            
    
        if (event.button == 3):
            if self.text_flag:
                self.text_flag = 0
                self.erase_ax0()
                # self.prepare_ax0()
                self.ax[0].set_title(self.title1 + "\n{}".format(self.time_points[self.curr_it].astype(datetime_type)),
                                     fontweight="bold")
                self.ax[0].scatter(self.special_point[0], self.special_point[1], 
                                   marker = '*', s = 100, color = 'red', zorder = 100)
                map_data = self.data_array[:,:,self.curr_it]
                    
                self.ax[0].pcolor(self.x_points, self.y_points, map_data.transpose(), alpha=0.5, 
                             shading = 'auto', cmap='Blues_r', vmin = -2, vmax=0)
              
            elif (len(self.x_points) * len(self.y_points) <= 250):
                self.text_flag = 1
                for ix in range(len(self.x_points)):
                    for iy in range(len(self.y_points)):
                        self.ax[0].text(self.x_points[ix], self.y_points[iy], 
                                        round(self.data_array[ix][iy][self.curr_it],1), 
                                        va = "center", ha = "center", alpha=0.5)

        plt.draw()
    
    def save_map(self):
        map_data = self.data_array[:,:,self.curr_it]
        time_plain = self.time_points[self.curr_it].astype(datetime_type)
        time_plain = str(time_plain).replace(':', '')
        with open('{}_map_{}.csv'.format(self.parameter_name, time_plain), 'w', newline='') as csvfile:
            csv_writer = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_NONE)
            csv_writer.writerow(["#longitude latitude {}".format(self.parameter_name)])
            for ix in range(len(self.x_points)):
                for iy in range(len(self.y_points)):
                    csv_writer.writerow([self.x_points[ix], self.y_points[iy], map_data[ix][iy]])  
        # extent = self.ax[0].get_window_extent().transformed(self.fig.dpi_scale_trans.inverted())
        self.fig.savefig('{}_map_{}.png'.format(self.parameter_name, 
                                                time_plain), bbox_inches="tight")
        
    def save_interactive_map(self):
        np.savez('{}_interactive_map_{}.npz'.format(self.parameter_name, time.time()), 
                 xmin = self.xmin, xmax = self.xmax, 
                 ymin = self.ymin, ymax = self.ymax, x_points = self.x_points, y_points = self.y_points, 
                 RTL = self.data_array, time_points = self.time_points, metainfo = self.metainfo, 
                 special_point = self.special_point, special_point_label = self.special_point_label)
    def save_current_map(self):
        map_data = self.data_array[:,:,self.curr_it]
        time_plain = self.time_points[self.curr_it].astype(datetime_type)
        time_plain = str(time_plain).replace(':', '')
        np.savez('{}_map_{}.npz'.format(self.parameter_name, 
                time_plain), 
                 xmin = self.xmin, xmax = self.xmax, 
                 ymin = self.ymin, ymax = self.ymax, x_points = self.x_points, y_points = self.y_points, 
                 RTL = map_data, time_moment = self.time_points[self.curr_it], 
                 metainfo = self.metainfo, special_point = self.special_point, 
                 special_point_label = self.special_point_label)
        
    def on_press(self, event):
    
        if event.key == 'e':
            self.save_map()
        if event.key == 'x':
            self.save_interactive_map()
        if event.key == 'z':
            self.save_current_map()    
        
        plt.draw()