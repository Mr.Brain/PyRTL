import numpy as np
import matplotlib.pyplot as plt
from rtl_time_manager import RTL_time_manager
import datetime as dt

# Script plots RTL time graphs in EQ source for each EQ with
# magnitude or ks greater than threshold value

# ======================== USER-DEFINED PARAMETERS ======================

CATALOG = './catalogs/catemsd_all_noaft.txt'
ks_threshold = 14.9     # filter for events to process
SHOW_EQ_ARROW = True
RTL_THRESHOLD = -2    # set to False if you don't want to draw threshold                   

H_TOP = 0       # minimum event depth (for filtering catalogue)
H_BOTTOM = 200  # maximun event depth (for filtering catalogue)
M_MIN = 9       # minimum magnitude or energy class (for filtering catalogue)

# =======================================================================

plt.close('all')
manager = RTL_time_manager()
manager.read_catalogue(CATALOG)
T_MIN = manager.get_start_date()
T_MIN = str(T_MIN)[:10].replace('-','')
time_data, lons, lats, ks_data  = manager.get_events_by_threshold(ks_threshold)

for i in range(len(time_data)):
    T_MAX = str(time_data[i]-432000000)[:10].replace('-','')  # this strange offset - time before the main shock,
                                                              # the right border of calculations
    manager.configure_params(
                             start_time = dt.datetime.strptime(T_MIN, "%Y%m%d"),
                             stop_time = dt.datetime.strptime(T_MAX, "%Y%m%d"),
                             time_step = 15,
                             point_longitude = lons[i],
                             point_latitude = lats[i],
                             M_min = M_MIN, h_min = H_TOP, h_max=H_BOTTOM
                             )
    manager.read_catalogue(CATALOG)
    manager.filter_events()
    manager.calc_R()
    manager.calc_T()
    manager.calc_L()
    manager.detrend_R()
    manager.detrend_T()
    manager.detrend_L()
    manager.calc_RTL()
    manager.normalize_RTL()
    manager.plot_RTL(SHOW_EQ_ARROW, RTL_THRESHOLD)