# Script for calculating RTL maps in specified area
# To achive full functional write
# "%matplotlib qt" in IPython console

UI_GRAPHICS = "DEARPYGUI" # DEARPYGUI or MATPLOTLIB

from rtl_time_manager import RTL_time_manager
import numpy as np
import argparse
import time as tm
import os

if UI_GRAPHICS == "DEARPYGUI":
    print("Using DEARPYGUI graphics")
    from dpg_ui_maps_manager import plot_interactive
elif UI_GRAPHICS == "MATPLOTLIB":
    print("Using MATPLOTLIB graphics")
    from mpl_ui_maps_manager import plot_interactive
else:
    UI_GRAPHICS = "DEARPYGUI"
    raise RuntimeWarning("Incorrect user interface specified. Switching to DEARPYGUI")
    from dpg_ui_maps_manager import plot_interactive

import math
import copy

# General parameters for RTL-maps configuration

# ======================== USER-DRFINED PARAMS ===========================

LON_NMAX, LAT_NMAX = 3, 3
CALC_STEP_DAYS = 30

# =============== DEFAULT PARAMETERS ===============

default_metafile = os.path.join('metafiles','kam_test.meta')

# ==================================================

title_text1 = "Параметр RTL"
title_text2 = "Параметр RTL в точке"

def calculate_maps(metafile, lon_ndots, lat_ndots, auto_range = True, 
                   auto_range_scale_km = None, 
                   time_step = CALC_STEP_DAYS, longitude_range = [None, None], 
                   latitude_range = [None, None], silent = False, 
                   save_maps = False, return_maps = False,
                   params_dict = None):
    

    RTL = []
    
    manager = RTL_time_manager()
    manager.time_step = time_step
    manager.apply_meta_data(metafile)
    
    if (params_dict!=None):
        manager.set_parameters_by_dict(params_dict)

    if auto_range:
        if (auto_range_scale_km == None):
            lon_start, lon_stop = int(manager.point_longitude)-8, int(manager.point_longitude)+8 
            lat_start, lat_stop = int(manager.point_latitude)-6, int(manager.point_latitude)+6
        else:
            l_km = auto_range_scale_km
            lon_start, lon_stop = int(manager.point_longitude)-0.5 * 111.3 * math.cos(manager.point_latitude)/l_km,\
                                 int(manager.point_longitude)+0.5 * 111.3 * math.cos(manager.point_latitude)/l_km 
            lat_start, lat_stop = int(manager.point_latitude)-0.5*(111/l_km),\
                                    int(manager.point_latitude)+0.5*(111/l_km)
    else:
        lon_start, lon_stop = longitude_range[0], longitude_range[1]
        lat_start, lat_stop = latitude_range[0], latitude_range[1]
        
    assert((lon_start != None) and (lon_stop != None))
    assert((lat_start != None) and (lat_stop != None))

    lon_dots = np.linspace(lon_start, lon_stop, 2*lon_ndots+1)[1::2]   # creating calc points 
    lat_dots = np.linspace(lat_start, lat_stop, 2*lat_ndots+1)[1::2]   # according to map borders

    print("Longitude range: {} - {}".format(lon_start, lon_stop))
    print("Latitude range: {} - {}".format(lat_start, lat_stop))
    print("Longitude points: {}, latitude points: {}".format(lon_ndots, lat_ndots))

    manager.filter_events()
    EQ_LON = manager.point_longitude
    EQ_LAT = manager.point_latitude
    
    manager.print_parameters()
    tic0 = tm.perf_counter()
    cnt = 0
    
    for x in lon_dots:
        temp = []
        n=0
        print("Completed {}%".format(round(100 * cnt/len(lon_dots),2)))
        for y in lat_dots:
            manager.reload_point(x,y)
            manager.RTL_full_process(silent = True)
            temp.append(manager.RTL)
            n+=1
        cnt+=1
        
        RTL.append(temp)
    
    tic1 = tm.perf_counter()
    print("Maps calculation time: {} s".format(round(tic1 - tic0, 4)))
    
    
    calculation_metainfo = "H = {} - {} km   M_MIN = {}   Calc radius = {} km   Time step = {} days"\
        "   r0 = {} km   t0 = {} days   l0 = {} km   p = {}".format(manager.h_min, 
                    manager.h_max, manager.M_min, manager.calc_radius, manager.time_step,
                    manager.r0, manager.t0, manager.l0, manager.p)
                     
    if save_maps:
        np.savez('interactive_map_{}.npz'.format(tm.time()), xmin = lon_start, xmax = lon_stop, 
                 ymin = lat_start, ymax = lat_stop, x_points = lon_dots, y_points = lat_dots, 
                 RTL = RTL, time_points = np.array(manager.time_points, dtype = np.datetime64), metainfo = calculation_metainfo, 
                 special_point = (EQ_LON, EQ_LAT), special_point_label = " M"+str(manager.eq_magnitude) +" "+ manager.eq_time.strftime("%Y"))
             
    if (silent == False):        
        plot_interactive(lon_start, lon_stop, lat_start, lat_stop, 
                          lon_dots, lat_dots, np.array(RTL), np.array(manager.time_points, dtype = np.datetime64), 
                          (EQ_LON, EQ_LAT), calculation_metainfo, args.shapefile, 
                          title_text1, title_text2, "RTL", copy.deepcopy(manager), 
                          " M"+str(manager.eq_magnitude) +" "+ manager.eq_time.strftime("%Y"))
        
    return lon_dots, lat_dots, RTL, copy.deepcopy(manager)

def load_maps(maps_path):
    
    loaded_arrays = np.load(maps_path, allow_pickle = True)
    lon_start = float(loaded_arrays['xmin'])
    lon_stop = float(loaded_arrays['xmax'])
    lat_start = float(loaded_arrays['ymin'])
    lat_stop = float(loaded_arrays['ymax'])
    lon_dots = loaded_arrays['x_points']
    lat_dots = loaded_arrays['y_points']
    time_points = loaded_arrays['time_points']
    data_array = loaded_arrays['RTL']
    special_point = loaded_arrays['special_point']
    metainfo = loaded_arrays['metainfo']
    point_label = loaded_arrays['special_point_label']
    
    plot_interactive(lon_start, lon_stop, lat_start, lat_stop, 
                     lon_dots, lat_dots, np.array(data_array), time_points, 
                     special_point, metainfo, args.shapefile, title_text1, 
                     title_text2, "RTL", None, point_label)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
                        prog = 'RTL maps',
                        epilog = 'Author Telegram: and_rey_ka')
    
    parser.add_argument('-m', '--metafile', help = "metafile with calculation settings")
    parser.add_argument('-shp', '--shapefile', help = "file with custom coastline")
    parser.add_argument('-lons', '--longitude_range', help = "format - min:max")
    parser.add_argument('-lats', '--latitude_range', help = "format - min:max")
    parser.add_argument('-lon_points', '--longitude_points', help = "number of points by longitude")
    parser.add_argument('-lat_points', '--latitude_points', help = "number of points by latitude")
    parser.add_argument('-l', '--load_npz', help = "load pre-saved interactive maps")
    parser.add_argument('-s', '--save_interactive', help = "save interactive map after calculating", action = "store_true")
    parser.add_argument('-snt', '--silent', help = "do not show interactive maps", action = "store_true")
    
    args = parser.parse_args()
    print()

    if (args.load_npz == None):
        if (args.metafile):
            target_metafile = args.metafile
            print("Metafile : {}".format(args.metafile))
        else:
            target_metafile = default_metafile
            print("Metafile not specified. Using default: {}".format(default_metafile))
        
        if (args.longitude_range and args.latitude_range):
            lon_start = float(args.longitude_range.split(':')[0])
            lon_stop = float(args.longitude_range.split(':')[1])
            lat_start = float(args.latitude_range.split(':')[0])
            lat_stop = float(args.latitude_range.split(':')[1])
            auto_ranging = False
        else:
            lon_start, lon_stop, lat_start, lat_stop = None, None, None, None
            auto_ranging = True
        
        if (args.longitude_points):
            lon_nmax = int(args.longitude_points)
        else:
            lon_nmax = LON_NMAX
            
        if (args.latitude_points):
            lat_nmax = int(args.latitude_points)
        else:
            lat_nmax = LAT_NMAX
        
        calculate_maps(target_metafile, lon_nmax, lat_nmax, auto_range = auto_ranging, 
                       time_step = CALC_STEP_DAYS, longitude_range = [lon_start, lon_stop], 
                       latitude_range = [lat_start, lat_stop],
                       silent = args.silent, save_maps = args.save_interactive)
    else:
        load_maps(args.load_npz)
        
