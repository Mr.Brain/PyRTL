import sys
import os
from glob import glob
import csv

directory = 'params/'
out_file = open("out.csv", 'w')

csv_writer = csv.writer(out_file, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
f = open(directory + os.listdir(directory)[0])
data = f.readlines()
target_row = []
for line in data:
    target_row.append(line.split(' ')[0])
    
csv_writer.writerow(target_row)
f.close()


for filename in os.listdir(directory):
    filepath = os.path.join(directory, filename)
    f = open(filepath)
    data = f.readlines()
    target_row = []
    for line in data:
        if (line.split(' ')[1][-1] == '\n'):
            target_row.append(line.split(' ')[1][:-1])
        else:
            target_row.append(line.split(' ')[1])
    csv_writer.writerow(target_row)
    f.close()



out_file.close()