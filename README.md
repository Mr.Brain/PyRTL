PyRTL is a library for calculating the RTL seismicity parameter in space and time. 

Install project:
```
git clone --recursive https://gitlab.com/Mr.Brain/PyRTL
cd PyRTL
pip install -r requirements.txt
```

Software operates with meta-files containing settings for RTL calculation such as catalog filtering, calculation point (longitude, latitude), time range, RTL coefficients, and other additional information. Example of metafile is in ./metafiles folder. Default metafile is metafiles/kam_test.meta

Usage example for specified metafile:
```
python3 rtl_time.py -m ./path/to/metafile.meta
```

