import numpy as np
import dearpygui.dearpygui as dpg
from dearpygui_ext.themes import create_theme_imgui_light
import shapefile as shp
import csv
import time
from datetime import datetime as datetime_type
import os

x = np.linspace(0,10,150)

def plot_interactive(x1, x2, y1, y2, x_dots, y_dots,  RTL, time_points,
                     special_point, metainfo, shapefile, title1, title2, 
                     parameter_name, RTL_manager, point_label = ""):    
    maps_man = maps_manager()
    maps_man.set_map_boundaries(x1, x2, y1, y2)
    maps_man.set_grid_nodes(x_dots, y_dots)
    maps_man.set_target_data(RTL, time_points, metainfo, special_point)
    maps_man.set_titles(title1, title2, parameter_name)
    maps_man.set_point_label(point_label)
    maps_man.print_controls_info()
    maps_man.set_original_manager(RTL_manager)
    maps_man.prepare_axes(shapefile)
    

class maps_manager:
    
    def __init__(self):
        self.text_flag = 0
        self.curr_it = 0
        self.xmin = None
        self.xmax = None
        self.ymin = None
        self.ymax = None
        self.x_points = None
        self.y_points = None
        self.RTL = None
        self.time_points = None
        self.metainfo = None
        self.special_point = None
        self.title1 = None
        self.title2 = None
        self.parameter_name = None
        self.original_manager = None
        
    def set_original_manager(self, RTL_man):
        self.original_manager = RTL_man
        
    def is_point_inside(self, point):
        xflag = ((point[0] <= self.xmax) and (point[0] >= self.xmin))
        yflag = ((point[1] <= self.ymax) and (point[1] >= self.ymin))
        return (xflag and yflag)
        
    def map_click_callback(self, sender, app_data):
        print(f"Mouse Button ID: {app_data}")
        mouse_x, mouse_y = dpg.get_plot_mouse_pos()
        ix = np.argmin(abs(self.x_points - mouse_x))
        iy = np.argmin(abs(self.y_points - mouse_y))
        print(self.x_points[ix], self.y_points[iy])

        print(ix, iy)

        dpg.configure_item("RTL_graph",label = "Graph in point ({}, {})".format\
                           (round(self.x_points[ix],2), round(self.y_points[iy],2)))
        dpg.fit_axis_data("RTL_graph_x_axis")
        dpg.fit_axis_data("RTL_graph_y_axis")
        dpg.set_value('RTL_line_graph', [self.time_points_timestamp, self.RTL[ix][iy]])
        # dpg.fit_axis_data("RTL_graph_y_axis")
        # dpg.set_axis_limits("RTL_graph_y_axis", 0, 1.2*max(self.RTL[ix][iy]))
    
    def linegraph_click_callback(self, sender, app_data):
        print(f"Mouse Button ID: {app_data}")
        mouse_x, mouse_y = dpg.get_plot_mouse_pos()
        timedelta_seconds = self.time_points_timestamp[-1] - mouse_x
        timedelta_years = timedelta_seconds / 3600 / 24 / 365
        # ix = np.argmin(abs(self.x_points - mouse_x))
        # iy = np.argmin(abs(self.y_points - mouse_y))
        # print(self.x_points[ix], self.y_points[iy])

        print("CLICK: years before earthquake - {}".format(round(timedelta_years, 3)))

        # dpg.configure_item("RTL_graph",label = "Graph in point ({}, {})".format\
        #                    (round(self.x_points[ix],2), round(self.y_points[iy],2)))
        # dpg.fit_axis_data("RTL_graph_x_axis")
        # dpg.fit_axis_data("RTL_graph_y_axis")
        # dpg.set_value('RTL_line_graph', [self.time_points_timestamp, self.RTL[ix][iy]])
        # dpg.fit_axis_data("RTL_graph_y_axis")
        # dpg.set_axis_limits("RTL_graph_y_axis", 0, 1.2*max(self.RTL[ix][iy]))

    def update_map(self, sender):
        self.curr_it = dpg.get_value(sender)
        map_data = self.RTL[:,:,self.curr_it].transpose()[::-1]
        dpg.set_value('hm', [map_data.flatten(), [len(self.y_points),len(self.x_points)]])
        dpg.configure_item("hm_plot",label = "Date {}".format\
                           (self.time_points[self.curr_it].astype(datetime_type)))
        
    def print_controls_info(self):
        print()
        print("Press Z to save current map in NPZ format")
        print("Press E to save current map in CSV and PNG format")
        print("Press X to save all maps in NPZ format")
        
    def set_titles(self, t1, t2, parameter_name):
        self.title1 = t1
        self.title2 = t2
        self.parameter_name = parameter_name
    
    def set_point_label(self, label):
        self.special_point_label = label
    
    def set_map_boundaries(self, xmin, xmax, ymin, ymax):
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
    
    def set_grid_nodes(self, x_points, y_points):
        self.x_points = x_points
        self.y_points = y_points
        
    def set_target_data(self,  RTL, time_points, metainfo, special_point = (None, None)):
        self.RTL = RTL
        self.time_points = time_points
        
        self.metainfo = metainfo
        self.special_point = special_point
        self.time_points_timestamp = []

        for timeobj in self.time_points:
            timestamp = ((timeobj - np.datetime64('1970-01-01T00:00:00'))
             / np.timedelta64(1, 's'))
            self.time_points_timestamp.append(timestamp)

            
    def prepare_axes(self, shapefile):
        if shapefile:
            self.shapefile = shapefile
        else:
            self.shapefile = os.path.join('shapefiles','NaturalEarth_MediumScale','ne_50m_land.shp')
        dpg.create_context()
        with dpg.font_registry():
            # first argument ids the path to the .ttf or .otf file
            default_font = dpg.add_font("TNR.ttf", 28)
            second_font = dpg.add_font("TNR.ttf", 15)

        # dpg.show_item_registry()
        dpg.create_viewport(title='PyRTL')

        light_theme = create_theme_imgui_light()
        dpg.bind_theme(light_theme)


        with dpg.item_handler_registry(tag = "wh") as handler:
            dpg.add_item_clicked_handler(callback=self.map_click_callback)
        with dpg.item_handler_registry(tag = "lineclick") as handler:
            dpg.add_item_clicked_handler(callback=self.linegraph_click_callback)
        with dpg.handler_registry() as handler:
            dpg.add_key_release_handler(key=dpg.mvKey_E, callback = self.save_map)
            dpg.add_key_release_handler(key=dpg.mvKey_X, callback = self.save_interactive_map)
            dpg.add_key_release_handler(key=dpg.mvKey_Z, callback = self.save_map_npz)

        with dpg.window(label="Example Window", tag="Primary Window"):            
            with dpg.group(horizontal=True):
                dpg.add_colormap_scale(tag = "cbar", min_scale=-2, max_scale=0, colormap = dpg.mvPlotColormap_Hot)
                with dpg.plot(label="Heatmap", tag = "hm_plot"):
                    dpg.bind_colormap(dpg.last_item(), dpg.mvPlotColormap_Hot)
                    sf = shp.Reader(self.shapefile)
                    dpg.add_plot_axis(dpg.mvXAxis, label="longitude", tag="x_axis")
                    dpg.add_plot_axis(dpg.mvYAxis, label="latitude", tag="y_axis")
 
                    dpg.set_axis_limits("x_axis", self.xmin, self.xmax)
                    dpg.set_axis_limits("y_axis", self.ymin, self.ymax)
                    initial_map_data = self.RTL[:,:,self.curr_it]
                    dpg.add_heat_series(initial_map_data.flatten(), len(self.y_points), len(self.x_points),
                                        tag = "hm", scale_min=-2, scale_max=0, 
                                        format = '', bounds_min = (self.xmin, self.ymin),
                                        bounds_max = (self.xmax, self.ymax), parent="y_axis")
                    dpg.add_colormap_scale(parent = "hm")
                    for shape in sf.shapeRecords():
                        for i in range(len(shape.shape.parts)):
                            i_start = shape.shape.parts[i]
                            if i==len(shape.shape.parts)-1:
                                i_end = len(shape.shape.points)
                            else:
                                i_end = shape.shape.parts[i+1]
                            x = [i[0] for i in shape.shape.points[i_start:i_end]]
                            y = [i[1] for i in shape.shape.points[i_start:i_end]]
                            flag_inside = 0
                            # plt.plot(x,y)
                            points_ = shape.shape.points[i_start:i_end]
                            for point in points_:
                                flag_inside = flag_inside or self.is_point_inside(point)
                            if flag_inside:
                                # dpg.draw_polygon(points_, color = [170,170,170,255])
                                dpg.add_line_series(x, y, parent = "y_axis")

                    dpg.add_scatter_series([self.special_point[0]],[self.special_point[1]], parent="y_axis", tag="scatter_eq_epi")
                    dpg.add_plot_annotation(label=self.special_point_label, default_value=self.special_point, 
                                            offset=((self.xmax-self.xmin)/7, -(self.ymax-self.ymin)/7), 
                                            color=[255,255, 255, 0], clamped = False)
                    dpg.add_scatter_series([],[],parent="y_axis", tag = "scatter_epicenters",)
                    
                with dpg.theme(tag="plot_theme"):
                    with dpg.theme_component(dpg.mvLineSeries):
                        dpg.add_theme_color(dpg.mvPlotCol_Line, (0, 0, 0, 255), category=dpg.mvThemeCat_Plots)
                    with dpg.theme_component(dpg.mvScatterSeries):
                        dpg.add_theme_color(dpg.mvPlotCol_Line, (255, 0, 0), category=dpg.mvThemeCat_Plots)
                        dpg.add_theme_style(dpg.mvPlotStyleVar_Marker, dpg.mvPlotMarker_Circle, category=dpg.mvThemeCat_Plots)
                        dpg.add_theme_style(dpg.mvPlotStyleVar_MarkerSize, 7, category=dpg.mvThemeCat_Plots)
                
                with dpg.theme(tag="scatter_epicenters_theme"):
                    with dpg.theme_component(dpg.mvScatterSeries):
                        dpg.add_theme_color(dpg.mvPlotCol_Line, (0, 0, 120,120), category=dpg.mvThemeCat_Plots)
                        dpg.add_theme_style(dpg.mvPlotStyleVar_Marker, dpg.mvPlotMarker_Circle, category=dpg.mvThemeCat_Plots)
                        dpg.add_theme_style(dpg.mvPlotStyleVar_MarkerSize, 2, category=dpg.mvThemeCat_Plots)
                        
                with dpg.theme(tag="scatter_eq_epi_theme"):
                    with dpg.theme_component(dpg.mvScatterSeries):
                        dpg.add_theme_color(dpg.mvPlotCol_Line, (0, 0, 255, 255), category=dpg.mvThemeCat_Plots)
                        dpg.add_theme_style(dpg.mvPlotStyleVar_Marker, dpg.mvPlotMarker_Asterisk, category=dpg.mvThemeCat_Plots)
                        dpg.add_theme_style(dpg.mvPlotStyleVar_MarkerSize, 12, category=dpg.mvThemeCat_Plots)
                        dpg.add_theme_style(dpg.mvPlotStyleVar_MarkerWeight, 4, category=dpg.mvThemeCat_Plots)
                        
                        # dpg.add_theme_style(dpg.mvStyleVar_Alpha, 0.2, category=dpg.mvThemeCat_Plots)
                  
                with dpg.theme(tag="line_graph_RTL_theme"):
                   with dpg.theme_component(dpg.mvLineSeries):
                       dpg.add_theme_color(dpg.mvPlotCol_Line, (255, 0, 0), category=dpg.mvThemeCat_Plots)         
                       dpg.add_theme_style(dpg.mvPlotStyleVar_LineWeight, 2, category=dpg.mvThemeCat_Plots)
                
                dpg.bind_item_theme("hm_plot", "plot_theme")
                dpg.bind_item_theme("scatter_epicenters", "scatter_epicenters_theme")
                dpg.bind_item_theme("scatter_eq_epi", "scatter_eq_epi_theme")

                with dpg.plot(tag = "RTL_graph"):
                    dpg.add_plot_legend(location = dpg.mvPlot_Location_SouthWest)
                    dpg.add_plot_axis(dpg.mvXAxis, tag="RTL_graph_x_axis", time=True)
                    with dpg.plot_axis(dpg.mvYAxis, label="RTL", tag="RTL_graph_y_axis"):
                        dpg.add_line_series(self.time_points_timestamp, self.RTL[0][0], label="RTL", tag = "RTL_line_graph")


                dpg.bind_item_theme("RTL_line_graph", "line_graph_RTL_theme")

            dpg.add_text(self.metainfo)
            dpg.add_slider_int(tag="time_slider", callback = self.update_map, 
                               min_value = 0, max_value = len(self.time_points)-1)
            dpg.bind_font(default_font)
            dpg.add_checkbox(label="Show epicenters", tag="checkbox_show_epic", 
                             callback = self.show_epicenters)
            dpg.add_text("Controls:\nZ - save current map in NPZ format\n"
                         "E - save current map in CSV format\n"
                         "X - save interactive map in NPZ format")

                

        dpg.bind_item_handler_registry("hm_plot", "wh")
        dpg.bind_item_handler_registry("RTL_graph", "lineclick")

        print(x)

            

        dpg.setup_dearpygui()
        dpg.show_viewport()
        dpg.maximize_viewport()

        dpg.set_primary_window("Primary Window", True)
        frame = 0
        while dpg.is_dearpygui_running():
            # print(frame)
            # print("WIDTH "+str(dpg.get_item_width("Primary Window")))
            # print(dpg.get_item_width("Primary Window").__class__)
            # print("HEIGHT "+str(dpg.get_item_height("Primary Window")))
            dpg.render_dearpygui_frame()
            frame+=1
            (frame == 10) and self.configure_initial_items_size(
                dpg.get_item_width("Primary Window"),
                dpg.get_item_height("Primary Window"))


        dpg.destroy_context()
        pass
    
    def show_epicenters(self, sender, app_data, user_data):
        if (self.original_manager != None):
            if (dpg.get_value(sender) == True):
                x_scatter = []
                y_scatter = []
                for event_params in self.original_manager.filtered_events:
                    x_scatter.append(event_params[1])
                    y_scatter.append(event_params[0])
                dpg.set_value("scatter_epicenters", [x_scatter, y_scatter])
            else:
                dpg.set_value("scatter_epicenters", [[], []])
            
                
    
    def configure_initial_items_size(self, primary_width, primary_height):
        dpg.configure_item("hm_plot", width=0.43*primary_width, height=0.43*primary_width)
        dpg.configure_item("RTL_graph", width=0.43*primary_width, height=0.43*primary_width)
        dpg.configure_item("cbar", width=0.05*primary_width, height=0.43*primary_width)
        dpg.configure_item("time_slider", width=0.95*primary_width)
        map_data = self.RTL[:,:,0].transpose()[::-1]
        dpg.set_value('hm', [map_data.flatten(), [len(self.y_points),len(self.x_points)]])
    
    def save_map(self):
        map_data = self.RTL[:,:,self.curr_it]
        time_plain = self.time_points[self.curr_it].astype(datetime_type)
        time_plain = str(time_plain).replace(':', '')
        with open('{}_map_{}.csv'.format(self.parameter_name, time_plain), 'w', newline='') as csvfile:
            csv_writer = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_NONE)
            csv_writer.writerow(["#longitude latitude {}".format(self.parameter_name)])
            for ix in range(len(self.x_points)):
                for iy in range(len(self.y_points)):
                    csv_writer.writerow([self.x_points[ix], self.y_points[iy], map_data[ix][iy]])  
        
    def save_interactive_map(self):
        np.savez_compressed('{}_interactive_map_{}.npz'.format(self.parameter_name, time.time()), 
                 xmin = self.xmin, xmax = self.xmax, 
                 ymin = self.ymin, ymax = self.ymax, x_points = self.x_points, y_points = self.y_points, 
                 RTL = self.RTL, time_points = self.time_points, metainfo = self.metainfo, 
                 special_point = self.special_point, special_point_label = self.special_point_label)
    
    def save_map_npz(self):
        map_data = self.RTL[:,:,self.curr_it]
        time_plain = self.time_points[self.curr_it].astype(datetime_type)
        time_plain = str(time_plain).replace(':', '')
        np.savez_compressed('{}_map_{}.npz'.format(self.parameter_name, 
                time_plain), 
                 xmin = self.xmin, xmax = self.xmax, 
                 ymin = self.ymin, ymax = self.ymax, 
                 x_points = self.x_points, y_points = self.y_points, 
                 RTL = map_data, time_moment = self.time_points[self.curr_it], 
                 metainfo = self.metainfo, special_point = self.special_point, 
                 special_point_label = self.special_point_label)
    
    def on_press(self, event):
    
        if event.key == 'e':
            self.save_map()
        if event.key == 'x':
            self.save_interactive_map()
        if event.key == 'z':
            self.save_map_npz() 
        