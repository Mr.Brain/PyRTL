from optimizer_core import optimizer
import argparse

# ======================== USER-DRFINED PARAMS ===========================

GRID_POINTS = (3, 3)
AREA_SIZE_KM = 100
TIME_OFFSET_YEARS = 2
MAX_ITERS = 20

# =============== DEFAULT PARAMETERS ===============

default_metafile = "metafiles/kam_test.meta"

# ==================================================

parser = argparse.ArgumentParser(
                    prog = 'RTL maps optimizer',
                    epilog = 'Author Telegram: and_rey_ka')

parser.add_argument('-m', '--metafile', help = "metafile with calculation settings")
parser.add_argument('-it', '--iterations', help = "optimizer iterations")

args = parser.parse_args()
print()

if (args.metafile):
    target_metafile = args.metafile
    print("Metafile : {}".format(args.metafile))
else:
    target_metafile = default_metafile
    print("Metafile not specified. Using default: {}".format(default_metafile))
    
if (args.iterations):
    iters = int(args.iterations)
else:
    iters = MAX_ITERS


opt = optimizer(metafile = target_metafile, max_iters = iters)
opt.set_params_range({"rtl_r0":[10,30,50,70,90],
                      "rtl_t0":[90,180,270,365]})
opt.print_settings()
score, candidate = opt.optimize()
print("Optimal candidate: {}".format(candidate))
print("Score: {}".format(score))