import maps
import numpy as np
import datetime
from scipy.optimize import curve_fit
import random

def r2_score(y_test : np.array, y_pred : np.array):
   assert(len(y_test)==len(y_pred))
   SS_res=np.sum((y_test-y_pred)**2)
   SS_tot=np.sum((y_test-np.mean(y_test))**2)
   return 1-(SS_res/SS_tot)

def Gauss(x : np.array, mean, sigma, A): 
    y = A*np.exp(-(x - mean)**2 / 2*sigma**2) 
    return y 

class optimizer:
    def __init__(self, metafile = None, max_iters = 20, grid_points = (3,3), 
                 grid_scale_km = 100, offset_years = 2):
        self.methods = ['RandomSearch']
        self.method = 'RandomSearch'
        self.max_iters = max_iters
        self.params_range = dict()
        self.metrics = "normal_fit_score"
        self.available_metrics = ["normal_fit_score", "cumulative_sum"]
        self.metafile = ""
        self.grid_points = grid_points # longitude, latitude
        self.grid_scale_km = grid_scale_km
        self.metafile = metafile
        self.offset_years = offset_years
        
    def set_metafile(self, metafile):
        self.metafile = metafile
        
    def set_params_range(self, params_range):
        self.params_range = params_range
        
    def get_random_candidate(self):
        candidate = dict()
        for key, value in self.params_range.items():
            candidate[key] = random.choice(value)
        return candidate
        
    def get_fitness_value(self, params):
        score = None
        lon_dots, lat_dots, RTL, manager =  maps.calculate_maps(self.metafile, self.grid_points[0],
                                            self.grid_points[1], auto_range=True, params_dict = params, 
                                            auto_range_scale_km=self.grid_scale_km, silent=True)
        
        time_points = np.array(manager.time_points, dtype = np.datetime64)
        threshold_time = manager.eq_time - datetime.timedelta(365 * self.offset_years)
        cut = time_points > threshold_time
        
        if self.metrics == "normal_fit_score":
            score = self.normal_fit_score(RTL, cut)
        if self.metrics == "cumulative_sum":
            score = self.cumulative_sum_score(RTL, cut)
        return score
            
    def normal_fit_score(self, RTL, cut):
        Q = 0
        for i in range(self.grid_points[0]):
            for j in range(self.grid_points[1]):
                temp_RTL = np.array(RTL[i][j][cut])
                # plt.plot(temp_RTL)
                try:
                    parameters, covariance = curve_fit(Gauss, range(len(temp_RTL)), temp_RTL, maxfev=10000)
                    fit_mean = parameters[0] 
                    fit_sigma = parameters[1]
                    fit_A = parameters[2] 
                    fit_y = Gauss(range(len(temp_RTL)), fit_mean, fit_sigma, fit_A)
                    # plt.plot(fit_y)
                    # plt.show()
                    Q += max(0, r2_score(temp_RTL, fit_y))
                except:
                    Q += 0
        return Q
    
    def cumulative_sum_score(self, RTL, cut):
        Q = 0
        for i in range(self.grid_points[0]):
            for j in range(self.grid_points[1]):
                temp_RTL = np.array(RTL[i][j][cut])
                Q += np.sum(temp_RTL)
        return -Q
    
    def write_optimized_metafile(self, params):
        f = open(self.metafile)
        optimized_file_name = self.metafile[:-5] + "_optimized.meta"
        fw = open(optimized_file_name, 'w')
        params_vocab = {}
        meta_lines = f.readlines()
        meta_lines = [line.split(':') for line in meta_lines]
        for line in meta_lines:
            if len(line) == 2:
                if line[0] in params.keys():
                    params_vocab[line[0]] = params[line[0]]
                else:
                    params_vocab[line[0]] = line[1][:-1]
            else:
                pass
        for key, value in params_vocab.items():
            fw.write("{}:{}\n".format(key,value))
        print("Optimal parameters written in file {}".format(optimized_file_name))
        
        
    
    def RandomSearch(self):
        
        objective_function = []
        candidates = []
        
        for it in range(self.max_iters):
            print()
            print("=" * 15 + " ITERATION {} ".format(it) + "=" * 15)
            print()
            candidate = self.get_random_candidate()
            candidates.append(candidate)
            score = self.get_fitness_value(candidate)
            objective_function.append(score)
            print("SCORE: {}".format(score))
        
        objective_function = np.array(objective_function)
        idx = np.argmax(objective_function)
        
        return objective_function[idx], candidates[idx]
    
    def print_settings(self):
        print("Maximum optimizer iterations : {}".format(self.max_iters))
        print("Metrics: {}".format(self.metrics))
        print("Method: {}".format(self.method))
        print("Optimizing parameters: {}".format(self.params_range))
    
    def optimize(self):
        print("Optimization started")
        if self.method == "RandomSearch":
            score, candidate = self.RandomSearch()
        self.write_optimized_metafile(candidate)
        return score, candidate
            
        
    