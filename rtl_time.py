# Script for plotting the RTL time graphs

from rtl_time_manager import RTL_time_manager
import datetime as dt
import argparse
import os

# ============ USER - DEFINED PARAMS =================

CATALOG = os.path.join('db_catalogs','Turkey','Turkey_ANSS_M2_noaft.txt')
ENERGY_SCALE = 'Ms'

H_TOP = 0      
H_BOTTOM = 80
M_MIN = 3.5

EQ_LON = 37.021
EQ_LAT = 37.225
T_MAX = '20230206' # earthquake time

R0 = 70
T0 = 800

SHOW_EQ_ARROW = True
RTL_THRESHOLD = -2    # set to False if you don't want to draw threshold

# ====================================================

parser = argparse.ArgumentParser(
                    prog = 'RTL time graphs',
                    epilog = 'Author Telegram: and_rey_ka')

parser.add_argument('-m', '--metafile')
parser.add_argument('-s', '--silent', action='store_true')
parser.add_argument('-npz', '--export_npz', action='store_true')
parser.add_argument('-p', '--point', help = "calculation point in format longitude:latitude")
args = parser.parse_args()

manager = RTL_time_manager()
if args.metafile:
    manager.apply_meta_data(args.metafile)
else:
    manager.set_catalog(CATALOG)
    manager.read_catalog()
    manager.configure_params(
                             start_time = "CATALOG_START_TIME",
                             eq_time = dt.datetime.strptime(T_MAX, "%Y%m%d"),
                             time_step = 20,
                             point_longitude = EQ_LON,
                             point_latitude = EQ_LAT,
                             M_min = M_MIN, h_min = H_TOP, h_max=H_BOTTOM,
                             r0=R0, t0=T0, calc_rad = R0 * 2, T_wind = T0 * 2, 
                             energy_scale=ENERGY_SCALE
                             )
    
manager.filter_events()
if args.point != None:
    override_lon = float(args.point.split(':')[0])
    override_lat = float(args.point.split(':')[1])
    print(override_lon, override_lat)
    manager.reload_point(override_lon, override_lat)
    
manager.print_parameters()

manager.pre_calculation_checks()
manager.RTL_full_process()
manager.plot_RTL(SHOW_EQ_ARROW, RTL_THRESHOLD, save_plot=True, 
                 silent = args.silent)
manager.export_csv()

if args.export_npz:
    manager.export_npz(time_format="years")