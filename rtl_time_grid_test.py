# Script for plotting the RTL time graphs

from rtl_time_manager import RTL_time_manager
import datetime as dt
import numpy as np
import os
import argparse

# ============ USER - DEFINED PARAMS =================

CATALOG = os.path.join('db_catalogs','NorthCalifornia', 'north_cal_noaft_M2_2023.txt')
LOAD_EVENTS_FROM_FILE = False
T_MIN = 'CATALOG_START_TIME' # time period to filter catalogue
T_MAX = '19901011' # earthquake time
EQ_LON = -119
EQ_LAT = 38

# ============ ranges ===========
H_TOP_RANGE = [0]
H_BOTTOM_RANGE = [80] 

M_MIN_RANGE = np.linspace(2, 3.5, 31)
# R0_RANGE =  np.linspace(1, 100, 31)
R0_RANGE =  [20]
T0_RANGE =  [120]
#T0_RANGE =  np.linspace(10, 600, 31)
# ===============================

SHOW_EQ_ARROW = True
RTL_THRESHOLD = -2    # set to False if you don't want to draw threshold
# ====================================================

save_basepath = "correlation_results"

if not os.path.exists(save_basepath):
    os.mkdir(save_basepath)

parser = argparse.ArgumentParser(
                    prog = 'RTL time graphs autotest',
                    epilog = 'Author Telegram: and_rey_ka')

parser.add_argument('-m', '--metafile')
parser.add_argument('-snt', '--silent', action='store_true')
#parser.add_argument('-npz', '--export_npz', action='store_true')
#parser.add_argument('-p', '--point', help = "calculation point in format longitude:latitude")
args = parser.parse_args()
manager = RTL_time_manager()

if args.metafile:
    manager.apply_meta_data(args.metafile)
else:
    manager.set_catalog(CATALOG)
    manager.read_catalog()
    if T_MIN == "CATALOG_START_TIME":
        start_time = "CATALOG_START_TIME"
    else:
        start_time = dt.datetime.strptime(T_MIN, "%Y%m%d")
    manager.configure_params(
                             start_time = start_time,
                             eq_time = dt.datetime.strptime(T_MAX, "%Y%m%d"),
                             time_step = 15,
                             point_longitude = EQ_LON,
                             point_latitude = EQ_LAT,
                             M_min = M_MIN_RANGE[0], h_min = H_TOP_RANGE[0], h_max=H_BOTTOM_RANGE[0],
                             r0=R0_RANGE[0], t0=T0_RANGE[0], calc_rad = 130, T_wind=700
                             )
name_prefix = ""
if args.metafile:
    name_prefix = args.metafile.replace('/','').replace('\\','').split('.')[0]
    print(name_prefix)
    os.mkdir(os.path.join(save_basepath, name_prefix))

for H_TOP in H_TOP_RANGE:
    for H_BOTTOM in H_BOTTOM_RANGE:
        for M_MIN in M_MIN_RANGE:
            for R0 in R0_RANGE:
                for T0 in T0_RANGE:
                    manager.set_parameters_by_dict({"rtl_r0": R0, "rtl_t0": T0, "rtl_h_top": H_TOP, "rtl_h_bottom": H_BOTTOM, "rtl_M_min": M_MIN})
                    manager.filter_events()
                    manager.print_parameters()
                    manager.pre_calculation_checks()
                    manager.RTL_full_process()
                    manager.plot_RTL(SHOW_EQ_ARROW, RTL_THRESHOLD, save_plot = True, silent = args.silent)
                    
                    
                    if args.metafile:
                        np.savez(os.path.join(save_basepath, name_prefix, 'res_H{}-{}_R0_{}_T0_{}_MMIN_{}').format(
                        H_BOTTOM, H_TOP, str(round(R0)).rjust(4, '0'), str(round(T0)).rjust(4, '0'), str(round(M_MIN,2)).replace('.','').ljust(3, '0')),  t=manager.time_points, rtl=manager.RTL)
                    else:
                        np.savez(os.path.join(save_basepath, 'res_H{}-{}_R0_{}_T0_{}_MMIN_{}').format(
                        H_BOTTOM, H_TOP, str(round(R0)).rjust(4, '0'), str(round(T0)).rjust(4, '0'), str(round(M_MIN,2)).replace('.','').ljust(3, '0')),  t=manager.time_points, rtl=manager.RTL)